# Documentación - Full Trip

## Índice

1. [Metodología a utilizar para el desarrollo del sistema](./Metodologia.md)
2. [Modelo de branching](./Branching.md)
3. [Toma de requerimientos](./Requerimientos.md)
4. [Historias de Usuario](./Historias.md)
5. [Descripción de tecnologías a utilizar]('./Tecnologías.md)
6. [Diagramas de Actividades](./Actividades.md)
7. [Listado y descripción de Microservicios](./Listado.md)
8. [Contratos de microservicios](./Contratos.md)
9. [Arquitectura del sistema](./Arquitectura.md)
10. [Diagrama de datos o de almacenamiento](./Almacenamiento.md)
11. [Descripción de la seguridad de la apliación](./Seguridad.md)