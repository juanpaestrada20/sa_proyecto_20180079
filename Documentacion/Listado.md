# Listado y descripción de Microservicios

## Usuarios

Microservicio que llevara las funcionalidades básicas de usuario de crear, actualizar, obtener y eliminar. También contaraá con el manejo de pasaporte COVID de los usuarios turistas

## Autenticación

Microservicio que llevará el control de inicio de sesión de usuarios y registro de los mismos al sistema

## Vuelos

Microservicio que se encargará de el manejo de los servicios de vuelos, con reseñas y comprobaciones de pasaporte COVID

## Autos

Microservicio para el manejo de renta de autos, tiempos de renta y devoluciones de renta de los mismos.

## Hoteles

Microeservicio que manejará la reservacion de habitaciones de hoteles con calendarios de reservación.

## Banco

Microservicios que menejará la informacion de tarjetas de crédito. La realización de pagos y la notificacion al usuario de la transacción.

## Resultados

Microservicio que recolectara la información necesaria para el administrador.