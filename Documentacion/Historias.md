# Historias de Usuario

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.1</th>
            <th colspan="4" align="center">Registro de Usuario</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario sin registro <b>quiero</b> tener un formulario para registrarse y seleccionar mi rol <b>para</b> que la aplicación tenga mi información y me deje utilizar la aplicación</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Usuario registrado con rol</li>
                    <li>Tener acceder al sistema</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 1</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 1</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.2</th>
            <th colspan="4" align="center">Login de Usuario</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario registradao <b>quiero</b>  poder ingresar mis credenciales <b>para</b> hacer uso de la aplicación dependiendo mi rol asignado.</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Mostrar módulos a los cuales tiene acceso según su roll</li>
                    <li>Tener acceder al sistema</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 1</td>
            <td colspan="1">Dependiente de: 1</td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 1</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.3</th>
            <th colspan="4" align="center">Registro de Administrador</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario administrador <b>quiero</b>  tener un formulario para registrar otros usuarios con rol de administrador <b>para</b> que tengan los mismos permisos que tengo.</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Usuario registrado con rol de administrador</li>
                    <li>Tener acceder al sistema</li>
                    <li>Acceso a módulos de admin</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 1</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 2</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.4</th>
            <th colspan="4" align="center">Registro de calendario</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b>  usuario de hotel <b>quiero</b> un módulo para configurar el calendario <b>para</b> los usuarios turistas tengan la información correcta al reservar.</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Calendario registrado</li>
                    <li>Usuarios con vista de calendario</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 2</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 2</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.5</th>
            <th colspan="4" align="center">Ver calendario de reservaciones</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario turista <b>quiero</b> poder seleccionar un hotel y poder ver el calendario <b>para</b> ver fechas de reservación disponible</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Usuario turista con acceso a hoteles</li>
                    <li>Mostrar de forma amigable las fechas disponibles.</li>
                    <li>No dejar reservar en fecha no disponible</li>
                    <li>Dejar reservar en fecha disponible</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 3</td>
            <td colspan="1">Dependiente de: 4</td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 2</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.6</th>
            <th colspan="4" align="center">Ver listado de hoteles</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario turista <b>quiero</b> tener un espacio para ver listado de hoteles registrados con posibilidad de filtrar <b>para</b> tener una mejor búsqueda al querer encontrar un hotel a reservar</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Acceder a listado de hoteles disponibles</li>
                    <li>Mostrar resultados de listado de hoteles filtrados</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 2</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 3</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.7</th>
            <th colspan="4" align="center">Registrar tarjeta de crédito</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario turista <b>quiero</b>  tener un espacio para ver listado de hoteles registrados con posibilidad de filtrar <b>para</b> tener una mejor búsqueda al querer encontrar un hotel a reservar</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Acceder a listado de hoteles disponibles</li>
                    <li>Mostrar resultados de listado de hoteles filtrados</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 2</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 3</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.8</th>
            <th colspan="4" align="center">Reservar hotel</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario turista <b>quiero</b> poder realizar la reservación de un hotel <b>para</b>realizada para las fechas deseadas </td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Realizar reservación</li>
                    <li>Actualizar el calendario de reservaciones</li>
                    <li>Verificar estado de tarjeta de crédito</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 4</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 7</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.9</th>
            <th colspan="4" align="center">Registrar autos</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario de autos <b>quiero</b> un módulo para autos disponibles en reserva <b>para</b> los usuarios turistas tengan la información correcta al reservar.</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Auto registrado</li>
                    <li>Usuarios con vista de auto</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 2</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 2</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.10</th>
            <th colspan="4" align="center">Ver autos disponibles</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario turista <b>quiero</b> oder seleccionar un auto y poder ver disponibilidad <b>para</b> ver fechas de reservación disponible</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Usuario turista con acceso a autos</li>
                    <li>Mostrar de forma amigable las fechas disponibles.</li>
                    <li>No dejar reservar en fecha no disponible</li>
                    <li>Dejar reservar en fecha disponble</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 1</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 1</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.11</th>
            <th colspan="4" align="center">Ver listado de autos</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario turista <b>quiero</b> tener un espacio para ver listado de autos registrados con posibilidad de filtrar <b>para</b> tener una mejor búsqueda al querer encontrar un auto a reservar</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Acceder a listado de autos disponibles</li>
                    <li>Mostrar resultados de listado de autos filtrados</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 2</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 3</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.12</th>
            <th colspan="4" align="center">Reservar auto</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario turista <b>quiero</b> poder realizar la reservación de un auto <b>para</b>realizada para las fechas deseadas </td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Realizar reservación</li>
                    <li>Actualizar el calendario de reservaciones</li>
                    <li>Verificar estado de tarjeta de crédito</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 4</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 7</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.14</th>
            <th colspan="4" align="center">Registrar pasaporte COVID</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario turista <b>quiero</b> un módulo para registrar mi información acerca de las vacunas de COVID <b>para</b> poder ser utilizada en la compra de viajes.</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Guardar información de vacunas covid</li>
                    <li>Posibilidad de actualizar datos de covid con vacuna nueva</li>
                    <li>Generación de certificado con información registrada.</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 3</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 5</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.15</th>
            <th colspan="4" align="center">Registrar vuelos</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario <b>quiero</b>  un módulo para registrar vuelos <b>para</b> los usuarios turistas tengan la información de vuelos</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Vuelo registrado</li>
                    <li>Usuarios con vista de vuelos</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 1</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 6</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.16</th>
            <th colspan="4" align="center">Seleccionar vuelos</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario turista <b>quiero</b> ver los vuelos diponibles <b>para</b> viajar al hotel que tengo reservado</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Lista de vuelos</li>
                    <li>Vuelos con información completa</li>
                    <li>Verificación de pasaporte COVID al seleccionar vuelo</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 3</td>
            <td colspan="1">Dependiente de: 14, 15</td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 5</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.17</th>
            <th colspan="4" align="center">Calificación de servicios</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario turista <b>quiero</b> poder calificar el servicio que reserve <b>para</b> retroalimentar a la empresa sobre su servicio y otros usuario sepan mas del servicio</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Registro de calificación</li>
                    <li>Al mostrar información del servicio mostrar reseñas</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 3</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 2</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.18</th>
            <th colspan="4" align="center">Reportes de Administrador</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> administrador <b>quiero</b> ver información resumida de los datos en el sistema <b>para</b> ver reportes y ayudar a la toma de decisiones.</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Datos resumidos en forma de gráficas</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 5</td>
            <td colspan="1">Dependiente de: Información en el sistema</td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 7</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.19</th>
            <th colspan="4" align="center">Apliación móvil</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario turista <b>quiero</b> una aplicación móvil <b>para</b> la reserva de servicios</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Usuario Registrado</li>
                    <li>Servicios Registrados</li>
                    <li>Reservaciones realizadas</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 8</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 8</td>
        </tr>
    </tbody>
</table>

<table border style="border-color:black">
    <thead style="background-color: #5b95f9; color: black;">
        <tr>
            <th colspan="1">No.20</th>
            <th colspan="4" align="center">Historial de servicios</th>
        </tr>
    </thead>
    <tbody style="background-color: #e8f0fe; color: black;">
        <tr>
            <td colspan="5" align="center"><b>Como</b> usuario turista <b>quiero</b> ver un historial de servicios reservados ordenados por fecha <b>para</b> llevar control de los servicios que he utilizado</td>
        </tr>
        <tr>
            <td colspan="5">
                <b>Criterios de Aceptación</b>
                <ul>
                    <li>Usuario registrado</li>
                    <li>Usuario con servicios reservados</li>
                    <li>Listado ordenado por fecha</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">Estimación: 2</td>
            <td colspan="1">Dependiente de: </td>
        </tr>
        <tr>
            <td colspan="5">Prioridad: 2</td>
        </tr>
    </tbody>
</table>
