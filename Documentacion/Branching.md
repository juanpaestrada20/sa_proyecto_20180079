# Modelo de branching

## Herramienta a Utilizar

GitFlow

GifFlow es un modelo de ramificación de Git alternativo  que implica el uso de ramas de caracterísiticas y varias ramas primarias. Gitflow tiene numerosas ramas de mayor duración y confirmaciones más grandes. Según este modelo, los desarrolladores crean una rama de función y retrasan la fusión con la rama a la principal hasta que se completa la función. Estas reamas de características de larga duración requieren más colaboración para fusionarse y tienen un mayor riesgo de desviarse de la rama principal. También pueden introducir actualizaciones contradictorias.

## Ramas a Utilizar

- **main**: Es la rama encargada para guardar las versiones estables del proyecto.
- **develop**: Es la rama encargada para guardar las versiones del código que se encuentra endesarrollo.
- **feature/&lt;funcion&gt;**: Es la rama encargada para realizar las nuevas funcionalidades del proyecto, las cuales salen de la rama develop y regresan a develop hasta tener la funcionalidad concluida.
- **hotfix**: Es la rama encargada para realizar cambios en caliente cuando el código ya se encuentra en producción (rama main) y también se agrega un tag correspondiente por el cambio realizado.
- **release/&lt;tag&gt;**: Es la rama encargada para preparar la siguiente versión recibida de la rama develop para enviarse a la rama main con su tag asociado.

![Ejemplificación de manejo de ramas](./images/gitflow.jpg)