# Arquitectura del sistema

Para la arquitectura se trabajará SOA que permite reutilizar sus elementos gracias a las interfaces de servicios que se comunican a través de una red con un lenguaje común. Un servicio es una unidad autónoma de una o más funciones del software diseñada para realizar una tarea específica, como recuperar cierta información o ejecutar una operación. Contiene las integraciones de datos y código que se necesitan para llevar a cabo una función completa y diferenciada. Se puede acceder a él de forma remota e interactuar con él o actualizarlo de manera independiente. Integrando los elementos del software que se implementan y se mantienen por separado, y permite que se comuniquen entre sí y trabajen en conjunto para formar aplicaciones de software en distintos sistemas.

El Bus de Servicios Empresariales (ESB) debe ser lo bastante robusto como para que permita administrar los cambios en los requerimientos sin que esto suponga en los servicios ya instalados incidencia alguna. Sistema de eventos e infraestructura deben ser capaces de conectar cualquier recurso de TI con independencia de qué tecnología emplee éste. Ayuda con la seguridad, ruteo del mensaje y es centralizado.

![Arquitectura del sistema](./images/arquitectura.png)
